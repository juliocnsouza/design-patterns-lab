/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns.sigleton;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juliocnsouza
 */
public class ConfiguracoesSistema {

    private volatile static ConfiguracoesSistema singleInstance;
    private List<String> propriedades;

    private ConfiguracoesSistema() {
        propriedades = new ArrayList<>();
    }

    public static ConfiguracoesSistema getInstance() {
        if (singleInstance == null) {
            synchronized (ConfiguracoesSistema.class) {
                if (singleInstance == null) {
                    singleInstance = new ConfiguracoesSistema();
                }
            }
        }
        return singleInstance;
    }

    public List<String> getPropriedades() {
        return propriedades;
    }
}
