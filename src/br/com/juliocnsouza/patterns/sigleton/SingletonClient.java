/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns.sigleton;

import br.com.juliocnsouza.patterns.Client;

/**
 *
 * @author juliocnsouza
 */
public class SingletonClient implements Client {

    @Override
    public void show() {
        ConfiguracoesSistema config01 = ConfiguracoesSistema.getInstance();
        ConfiguracoesSistema config02 = ConfiguracoesSistema.getInstance();

        config01.getPropriedades().add("Prop add no config 01");
        config02.getPropriedades().add("Prop add no config 02");

        System.out.println("Mostrando props do config01 -> " + config01.getPropriedades());
        System.out.println("Mostrando props do config02 -> " + config02.getPropriedades());
    }
}
