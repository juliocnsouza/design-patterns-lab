/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns.abstract_factory;

import br.com.juliocnsouza.patterns.Client;

/**
 *
 * @author juliocnsouza
 */
public class ClienteSmartphone implements Client {
    
    private SmartphoneAbstractFactory factory;
    private AbstractSmartphone smartphone;
    
    @Override
    public void show() {
        factory = new IPhoneFactory();
        smartphone = factory.criarSmartphone();
        showSmartphone(smartphone);
        factory = new SamsungS4Factory();
        smartphone = factory.criarSmartphone();
        showSmartphone(smartphone);
    }
    
    private void showSmartphone(AbstractSmartphone smartphone) {
        System.out.println("\n\nSou Smarphone - implementação de: " + smartphone.getClass().getSimpleName());
        System.out.println("Meu OS é " + smartphone.getOS());
        System.out.println("Meu Acesso a internet: " + smartphone.acessarInternet());
        System.out.println("Faço ligações: " + smartphone.fazerLigacao());
        System.out.println("Minha lista de Apps ");
        for (String app : smartphone.apps()) {
            System.out.println("-> " + app);
        }
    }
}
