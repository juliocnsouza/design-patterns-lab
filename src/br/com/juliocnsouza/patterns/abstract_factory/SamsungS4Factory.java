/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns.abstract_factory;

/**
 *
 * @author juliocnsouza
 */
public class SamsungS4Factory implements SmartphoneAbstractFactory {

    @Override
    public AbstractSmartphone criarSmartphone() {
        return new SamsungGalaxy();
    }
}
