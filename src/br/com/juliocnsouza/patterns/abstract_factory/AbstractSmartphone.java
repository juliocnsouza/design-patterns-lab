/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns.abstract_factory;

import java.util.List;

/**
 *
 * @author juliocnsouza
 */
public interface AbstractSmartphone {

    public String fazerLigacao();

    public String acessarInternet();

    public List<String> apps();

    public String getOS();
}
