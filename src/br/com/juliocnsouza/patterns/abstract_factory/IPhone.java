/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns.abstract_factory;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author juliocnsouza
 */
public class IPhone implements AbstractSmartphone {

    @Override
    public String getOS() {
        return "iOS";
    }

    @Override
    public String fazerLigacao() {
        return "Via teclado ou comando de voz";
    }

    @Override
    public String acessarInternet() {
        return "Navegadores, redes sociais e muito mais";
    }

    @Override
    public List<String> apps() {
        return Arrays.asList("iTunes", "WhatApp", "Facebook");
    }
}
