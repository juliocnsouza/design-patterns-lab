/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.juliocnsouza.patterns;

import br.com.juliocnsouza.patterns.sigleton.SingletonClient;

/**
 *
 * @author juliocnsouza
 */
public class Main {

    public static void main(String[] args) {
        //Client c = new ClienteSmartphone();
        Client c = new SingletonClient();
        c.show();
    }
}
